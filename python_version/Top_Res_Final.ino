int RES_pin[32] = {22, 23, 24, 25, 26, 27, 28, 29,
                  30, 31, 32, 33, 34, 35, 36, 37, 
                  38, 39, 40, 41, 42, 43, 44, 45,
                  46, 47, 48, 49, 50, 51, 52, 53};

int north_up[4] = {22, 23, 24, 25};
int east_up[4] = {26, 27, 28, 29};
int south_up[4] = {30, 31, 32, 33};
int west_up[4] = {34, 35, 36, 37};
int north_down[4] = {38, 39, 40, 41};
int east_down[4] = {42, 43, 44, 45};
int south_down[4] = {2, 3, 4, 5};
int west_down[4] = {6, 7, 8, 9};

String incoming_msg; 
String component;
int order; 
int percent;

void setup() {
  Serial.begin(115200); //Starting serial communication

  // RESONATOR SETUP
  for (int i : RES_pin) {
    pinMode(i, OUTPUT);
  }
  for (int i : RES_pin) {
    digitalWrite(i, LOW);
  }

  pinMode(LED_BUILTIN, OUTPUT);
  
}
void loop() {
  // put your main code here, to run repeatedly:

  //Serial.println("Hello Pi");

  if (Serial.available()>0) {
    incoming_msg = Serial.readStringUntil(";");

    component = getValue(incoming_msg, ',', 0);
    
    if (component == "res") {
      if (incoming_msg.indexOf("[") == -1) {

        order = getValue(incoming_msg, ',', 1).toInt();
        percent = getValue(incoming_msg, ',', 2).toInt();
        
          if (order == 0) {
            res_control(north_up[0], percent); 
            res_control(north_up[1], percent); 
            res_control(north_up[2], percent); 
            res_control(north_up[3], percent); 
          } else if (order == 1) {
            res_control(east_up[0], percent); 
            res_control(east_up[1], percent); 
            res_control(east_up[2], percent); 
            res_control(east_up[3], percent); 
          } else if (order == 2) {
            res_control(south_up[0], percent); 
            res_control(south_up[1], percent); 
            res_control(south_up[2], percent); 
            res_control(south_up[3], percent); 
          } else if (order == 3) {
            res_control(west_up[0], percent); 
            res_control(west_up[1], percent); 
            res_control(west_up[2], percent); 
            res_control(west_up[3], percent); 
          } else if (order == 4) {
            res_control(north_down[0], percent); 
            res_control(north_down[1], percent); 
            res_control(north_down[2], percent); 
            res_control(north_down[3], percent); 
          } else if (order == 5) {
            res_control(east_down[0], percent); 
            res_control(east_down[1], percent); 
            res_control(east_down[2], percent); 
            res_control(east_down[3], percent); 
          } else if (order == 6) {
            res_control(south_down[0], percent); 
            res_control(south_down[1], percent); 
            res_control(south_down[2], percent); 
            res_control(south_down[3], percent); 
          } else if (order == 7) {
            res_control(east_down[0], percent); 
            res_control(east_down[1], percent); 
            res_control(east_down[2], percent); 
            res_control(east_down[3], percent); 
          }
      } else {

        percent = getValue(incoming_msg, ',', 5).toInt();
        
        if (incoming_msg[7] == "1") {
           
          for (int i=0; i < 4; i++) {
            res_control(north_up[i], percent);
          }
          for (int i=0; i < 4; i++) {
            res_control(east_up[i], percent);
          }
          for (int i=0; i < 4; i++) {
            res_control(south_up[i], percent);
          }
          for (int i=0; i < 4; i++) {
            res_control(west_up[i], percent);
          }

          Serial.println("OLA OLA OLA");
          
        } else if (incoming_msg[7] == "4") {
          for (int i=0; i < 4; i++) {
            res_control(north_down[i], percent);
          }
          for (int i=0; i < 4; i++) {
            res_control(east_down[i], percent);
          }
          for (int i=0; i < 4; i++) {
            res_control(south_down[i], percent);
          }
          for (int i=0; i < 4; i++) {
            res_control(west_down[i], percent);
          }
        }
      }
    }
  }
}

void res_control(int res_num, int res_percent) {
  //CHECK: CURRENTLY SET FOR 8way control

  if (res_percent < 1) {
      digitalWrite(res_num, LOW);
  } else if (res_percent == 1) {
      digitalWrite(res_num, HIGH);
  }

  //feedback
  //Serial.println(String(res_num) + ", " + String(res_pwm));
}

String getValue(String data, char separator, int index) {
    int found = 0;
    int strIndex[] = { 0, -1 };
    int maxIndex = data.length() - 1;

    for (int i = 0; i <= maxIndex && found <= index; i++) {
        if (data.charAt(i) == separator || i == maxIndex) {
            found++;
            strIndex[0] = strIndex[1] + 1;
            strIndex[1] = (i == maxIndex) ? i+1 : i;
        }
    }
    return found > index ? data.substring(strIndex[0], strIndex[1]) : "";
}
