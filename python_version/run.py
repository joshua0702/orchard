import serial
import socketio
import eventlet.wsgi
from flask import Flask


class SerialWrapper(object):
    ser = None
#serial.Serial('/dev/ttyACM1', 115200)

    def send_fog(self, n, percent):
        # res_num: (int) 1-32, odds are bottom, evens are top
        # percent: (int) 0-100%, in increments of 5%
        self.ser.write('res,' + str(n) + ',' + str(percent) + ';')

    def send_water(self, res_num, percent):
        # res_num: (int) 1-4, odds are bottom, evens are top
        # percent: (int) 0-100%, in increments of 5%
        self.ser.write('res,' + str(res_num) + ',' + str(percent) + ';')

    def send_led(self, wave, amp):
        # wave: (int) 1-6, one of six wavelengths in order of: 450, 465, 470, 635, 655, 670
        # amp:  (int) 0-255, in increments of 1
        self.ser.write('led,' + str(wave) + ',' + str(amp) + ';')

    def serial_read(self):
        # msg: should be string
        msg = self.ser.readline()

sio = socketio.Server()
#    serial = SerialWrapper()


@sio.on('connect', namespace='/chat')
def connect(sid, environ):
    print 'Handshake completed - socket is established'


@sio.on('disconnect', namespace='/chat')
def disconnect(sid, environ):
    print 'Handshake broken - socket is disconnected'


@sio.on('fog-request', namespace='/chat')
def on_fog_request(sid, data):
    n, state = parse_fog_request_data(data)
    if n is not None and state is not None:
        #self.serial.send_fog(n, percent)
        print 'Set resonator #{} to {}'.format(n , state)
    else:
        print 'Error parsing fog request\'s data: {}'.format(data)
        # self.sio.emit('temperature-report', x, room=sid, namespace='/chat')


def parse_fog_request_data(data):
    # An example of 'data' could be something like: "3-35", meaning fog resonator 3 to 35%
    if data:
        split = data.split(",")
        if len(split) == 2:
            IDs, state = split[:2]
            return [int(x) for x in IDs.split("-")], int(state)
    return None, None


@sio.on('LED-request', namespace='/chat')
def on_led_request(sid, data):
    for item in parse_led_request_data(data):
        id, percent = item.split('-')[:2]
        print 'Turn LED #{} to {}'.format(id, percent)


def parse_led_request_data(data):
    # An example of 'data' could be something like: "0-0.6,1-0.2", meaning led #0 to 60%, led #1 to 20%
    return data.split(',')


app = Flask(__name__)
if __name__ == '__main__':
    app = socketio.Middleware(sio, app)
    eventlet.wsgi.server(eventlet.listen(('', 8000)), app)
