import serial

ser = serial.Serial('/dev/ttyACM1', 115200)

def res_send(res_num, percent):
	# res_num: (int) 1-32, odds are bottom, evens are top
	# percent: (int) 0-100%, in increments of 5%
	
	ser.write('res,' + str(res_num) + ',' + str(percent) + ';')
	
def LED_send(wave, amp):
	# wave: (int) 1-6, one of six wavelengths in order of: 450, 465, 470, 635, 655, 670
	# amp:  (int) 0-255, in increments of 1
	
	ser.write('led,' + str(wave) + ',' + str(amp) + ';')
	
def pump_send(pump_num):
	# pump_num: (int) 1-4, 1: +10ppm EC, 2: -10ppm EC, 3: +0.1 pH, 4: -0.1 pH
	
	ser.write('pum,' + str(pump_num) + ';')
	
def fan_send(pwm):
	# pwm:  (int) 0-100, 5% increments  controls all pumps
	
	ser.write('fan,' + str(pwm) + ';')
	
def serial_read():
	# msg: should be string
	msg = ser.readline()
	
	if msg[0:3] == 'res':
