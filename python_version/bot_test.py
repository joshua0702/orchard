import serial
import time

ser = serial.Serial('/dev/ttyACM0', 115200)

def send_fog(n, percent):
    # res_num: (int) 1-32, odds are bottom, evens are top
    # percent: (int) 0-100%, in increments of 5%
    ser.write('res,' + str(n) + ',' + str(percent) + ';')

def send_water(res_num, percent):
    # res_num: (int) 1-4, odds are bottom, evens are top
    # percent: (int) 0-100%, in increments of 5%
    ser.write('pum,' + str(res_num) + ',' + str(percent) + ';')

def send_led(wave, amp):
    # wave: (int) 1-6, one of six wavelengths in order of: 450, 465, 470, 635, 655, 670
    # amp: ) 0-255, in increments of 1
    ser.write('led,' + str(wave) + ',' + str(amp) + ';')

def serial_read():
    # msg: should be string
    msg = ser.readline()
    return msg

pp = [1, 2, 3, 4]

for x in range(0, 3):
    send_water(pp[x], 100)
    print("..............")
    print(x)
    print("Sending data to " + str(pp[x]))
    time.sleep(2) 
    while ser.in_waiting !=0 :
        print("waiting...")
        print(ser.in_waiting)
        print(ser.readline())
        #print("Receiving: " + serial_read())

