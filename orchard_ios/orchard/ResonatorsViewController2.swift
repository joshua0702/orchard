//
//  ResonatorsViewController.swift
//  orchard
//
//  Created by Long Tran on 4/8/18.
//  Copyright © 2018 ltranco. All rights reserved.
//

import UIKit
import SceneKit

class ResonatorsViewController2: UIViewController {
    private let sceneView = SCNView(frame: CGRect(x: 0, y: 0, width: 300, height: 300))
    private let scene = SCNScene()

    override func loadView() {
        // Set up scene
        sceneView.scene = scene
        sceneView.backgroundColor = UIColor(displayP3Red: 16/255.0, green: 16/255.0, blue: 16/255.0, alpha: 1)
        sceneView.autoenablesDefaultLighting = true
        sceneView.allowsCameraControl = true
        
        // Set up camera
        let cameraNode = SCNNode()
        cameraNode.camera = SCNCamera()
        cameraNode.position = SCNVector3(x: 0, y: 0, z: 30)
        scene.rootNode.addChildNode(cameraNode)
        
        // Add core tube
        let core = SCNTube(innerRadius: 1.5, outerRadius: 2, height: 20)
        core.firstMaterial?.diffuse.contents = UIColor.darkGray
        let coreNode = SCNNode(geometry: core)
        coreNode.position = SCNVector3(x: 0, y: 0, z: 0)
        scene.rootNode.addChildNode(coreNode)
        
        // Add 4 resonators
        addResonator(v: SCNVector3(x: -10, y: 0, z: 0))
        addResonator(v: SCNVector3(x: 10, y: 0, z: 0))
        addResonator(v: SCNVector3(x: 0, y: 0, z: -10))
        addResonator(v: SCNVector3(x: 0, y: 0, z: 10))
        
        // set a rotation axis (no angle) to be able to
        // use a nicer keypath below and avoid needing
        // to wrap it in an NSValue
//        boxNode.rotation = SCNVector4(x: 1, y: 1, z: 0.0, w: 0.0)
        
        // animate the rotation of the torus
        let spin = CABasicAnimation(keyPath: "rotation.w") // only animate the angle
        spin.toValue = 2.0*Double.pi
        spin.duration = 10
        spin.repeatCount = HUGE // for infinity
//        boxNode.addAnimation(spin, forKey: "spin around")
        
        view = sceneView // Set the view property to the sceneView created here.
    }
    
    private func addResonator(v: SCNVector3) {
        let r = SCNCylinder(radius: 1.0, height: 10.0)
        r.firstMaterial?.diffuse.contents = UIColor.lightGray
        let rNode = SCNNode(geometry: r)
        rNode.position = v
        scene.rootNode.addChildNode(rNode)
        
        // Top level
        rNode.addChildNode(createPod(position: SCNVector3(x: 0, y: -3, z: 0.8),
                                     pivot: SCNMatrix4MakeRotation(Float.pi / 2, -1, 0, 0)))
        rNode.addChildNode(createPod(position: SCNVector3(x: 0, y: -3, z: -0.8),
                                     pivot: SCNMatrix4MakeRotation(Float.pi / 2, -1, 0, 0)))
        rNode.addChildNode(createPod(position: SCNVector3(x: -0.8, y: -3, z: 0),
                                     pivot: SCNMatrix4MakeRotation(Float.pi / 2, 0, 0, -1)))
        rNode.addChildNode(createPod(position: SCNVector3(x: 0.8, y: -3, z: 0),
                                     pivot: SCNMatrix4MakeRotation(Float.pi / 2, 0, 0, -1)))
        
        // Bottom level
        rNode.addChildNode(createPod(position: SCNVector3(x: 0, y: 3, z: 0.8),
                                     pivot: SCNMatrix4MakeRotation(Float.pi / 2, -1, 0, 0), bottomLevel: true))
        rNode.addChildNode(createPod(position: SCNVector3(x: 0, y: 3, z: -0.8),
                                     pivot: SCNMatrix4MakeRotation(Float.pi / 2, -1, 0, 0), bottomLevel: true))
        rNode.addChildNode(createPod(position: SCNVector3(x: -0.8, y: 3, z: 0),
                                     pivot: SCNMatrix4MakeRotation(Float.pi / 2, 0, 0, -1), bottomLevel: true))
        rNode.addChildNode(createPod(position: SCNVector3(x: 0.8, y: 3, z: 0),
                                     pivot: SCNMatrix4MakeRotation(Float.pi / 2, 0, 0, -1), bottomLevel: true))
    }
    
    private func createPod(position: SCNVector3, pivot: SCNMatrix4, bottomLevel: Bool = false) -> SCNNode {
        let p = SCNTube(innerRadius: 0.25, outerRadius: 0.5, height: 0.5)
        p.firstMaterial?.diffuse.contents = bottomLevel ? UIColor(displayP3Red: 1/255.0, green: 185/255.0, blue: 1, alpha: 1) :
            UIColor(displayP3Red: 4/255.0, green: 1, blue: 155/255.0, alpha: 1)
        let t1Node = SCNNode(geometry: p)
        t1Node.position = position
        t1Node.pivot = pivot
        return t1Node
    }
}
