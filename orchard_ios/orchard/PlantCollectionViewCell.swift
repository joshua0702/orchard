//
//  PlantCollectionViewCell.swift
//  orchard
//
//  Created by Long Tran on 4/9/18.
//  Copyright © 2018 ltranco. All rights reserved.
//

import UIKit

class PlantCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var plantImage: UIImageView!
    
    func setImage(img: UIImage?) {
        plantImage.image = img
    }
}
