//
//  OrchardSocketManager.swift
//  orchard
//
//  Created by Long Tran on 3/27/18.
//  Copyright © 2018 ltranco. All rights reserved.
//

import SocketIO

class OrchardSocketManager {
    static var shared = OrchardSocketManager()
    
    private init() {
    }
    
    private var manager: SocketManager?
    private var socket: SocketIOClient?
    
    func connectTo(url: String) {
        manager = SocketManager(socketURL: URL(string: url)!, config: [.log(false), .compress])
        socket = manager?.socket(forNamespace: "/chat")
        socket?.connect()
    }
    
    enum State: Int {
        case ON = 1
        case OFF = 0
    }
    
    func sendResonator(ID: Int, state: State) {
        socket?.emit("fog-request", with: ["\(ID),\(state.rawValue)"])
    }
    
    func sendResonators(IDs: [Int], state: State) {
        socket?.emit("fog-request", with: ["\(IDs.map({"\($0)"}).joined(separator: "-")),\(state.rawValue)"])
    }
    
    func send(LEDS: [(Int, Double)]) {
        socket?.emit("LED-request", with: [LEDS.map({"\($0.0)-\($0.1)"}).joined(separator: ",")])
    }
    
//        socket?.on("temperature-report") {data, ack in
//            self.label.text = "temperature is \(data)"
//        }
}

