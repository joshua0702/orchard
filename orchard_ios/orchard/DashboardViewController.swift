//
//  DashboardViewController.swift
//  orchard
//
//  Created by Long Tran on 4/7/18.
//  Copyright © 2018 ltranco. All rights reserved.
//

import UIKit
import Lottie

class DashboardViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UITextFieldDelegate {
    @IBOutlet weak var plantCollection: UICollectionView!
    private var selectedIndex: IndexPath?
    
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    private var dateFormatter: DateFormatter = DateFormatter()
    private var timeFormatter: DateFormatter = DateFormatter()
    
    @IBOutlet weak var connectButton: UIButton!
    @IBOutlet weak var ipField: UITextField!
    @IBOutlet weak var ecLabel: UILabel!
    @IBOutlet weak var pHLabel: UILabel!
    @IBOutlet weak var pwmLabel: UILabel!
    private var ec = 399 {
        didSet {
            ecLabel.text = "\(ec)"
        }
    }
    private var pH = 6.80 {
        didSet {
            pHLabel.text = String(format: "%.2f", pH)
        }
    }
    private var pwm = 75 {
        didSet {
            pwmLabel.text = "\(pwm)"
        }
    }
    
    @IBOutlet weak var temperature: UILabel!
    @IBOutlet weak var tempContainer: UIView!
    private var tempAnimation = LOTAnimationView(name: "temp.json")
    
    @IBOutlet weak var waterLevel: ProgressBarView!
    @IBOutlet weak var nutrientsLevel: ProgressBarView!
    @IBOutlet weak var pHUpLevel: ProgressBarView!
    @IBOutlet weak var pHDownLevel: ProgressBarView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        plantCollection.layer.borderColor = UIColor.white.cgColor
        plantCollection.layer.cornerRadius = 10
        plantCollection.layer.borderWidth = 4
        plantCollection.layer.masksToBounds = true
        plantCollection.backgroundColor = UIColor(displayP3Red: 14/255.0, green: 12/255.0, blue: 20/255.0, alpha: 1)
        
        ecLabel.text = "\(ec)"
        pHLabel.text = "\(pH)"
        pwmLabel.text = "\(pwm)"

        setUpClock()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        Timer.scheduledTimer(withTimeInterval: 0.2, repeats: false, block: {_ in
            self.waterLevel.setProgressTo(percentage: 94/100)
            self.nutrientsLevel.setProgressTo(percentage: 91/100)
            self.pHUpLevel.setProgressTo(percentage: 95/100)
            self.pHDownLevel.setProgressTo(percentage: 70/100)
            self.setTemp(t: 80)
            self.tempAnimation.play()
        })
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        waterLevel.reset()
        nutrientsLevel.reset()
        pHUpLevel.reset()
        pHDownLevel.reset()
        tempAnimation.animationProgress = 0
    }

    private func setUpClock() {
        dateFormatter.dateFormat = "EEEE, MMMM d, yyyy"
        timeFormatter.dateFormat = "h:mm:ss"
        
        updateClock()
        setUpTemp()

        Timer.scheduledTimer(withTimeInterval: 1, repeats: true, block: {_ in
            self.updateClock()
        })
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 25
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "plantCell", for: indexPath) as! PlantCollectionViewCell
        cell.setImage(img: UIImage(named: "\(indexPath.row + 1).png"))
        cell.backgroundColor = selectedIndex?.row == indexPath.row ? .darkGray : .clear
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedIndex = indexPath
        collectionView.reloadData()
    }
    
    private func setTemp(t: Int) {
        temperature.text = "\(t)°"
    }
    
    private func setUpTemp() {
        tempAnimation.animationSpeed = 3
        tempContainer.addSubview(tempAnimation)
        tempAnimation.translatesAutoresizingMaskIntoConstraints = false

        NSLayoutConstraint(item: tempAnimation, attribute: .centerX, relatedBy: .equal, toItem: tempContainer, attribute: .centerX, multiplier: 1, constant: 0).isActive = true
        NSLayoutConstraint(item: tempAnimation, attribute: .centerY, relatedBy: .equal, toItem: tempContainer, attribute: .centerY, multiplier: 1, constant: 0).isActive = true
        NSLayoutConstraint(item: tempAnimation, attribute: .width, relatedBy: .equal, toItem: tempContainer, attribute: .width, multiplier: 1, constant: 0).isActive = true
        NSLayoutConstraint(item: tempAnimation, attribute: .height, relatedBy: .equal, toItem: tempContainer, attribute: .height, multiplier: 1, constant: 0).isActive = true
        tempAnimation.play()
    }
    
    private func updateClock() {
        let d = Date()
        dateLabel.text = dateFormatter.string(from: d)
        timeLabel.text = timeFormatter.string(from: d)
    }
    
    @IBAction func onECUp(_ sender: Any) {
        ec += 1
    }
    
    @IBAction func onECDown(_ sender: Any) {
        ec -= 1
    }
    
    @IBAction func onpHUp(_ sender: Any) {
        if String(format: "%.2f", pH) == String(format: "%.2f", 8.0) {
            return
        }
        pH += 0.05
    }
    
    @IBAction func onpHDown(_ sender: Any) {
        if String(format: "%.2f", pH) == String(format: "%.2f", 5.0) {
            return
        }
        pH -= 0.05
    }
    
    @IBAction func onPWMUp(_ sender: Any) {
        if pwm == 100 {
            return
        }
        pwm += 5
    }
    
    @IBAction func onPWMDown(_ sender: Any) {
        if pwm == 0 {
            return
        }
        pwm -= 5
    }
    
    @IBAction func onConnect(_ sender: Any) {
        OrchardSocketManager.shared.connectTo(url: ipField.text ?? "")
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
