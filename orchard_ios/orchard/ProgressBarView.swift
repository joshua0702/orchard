//
//  ProgressBarView.swift
//  orchard
//
//  Created by Long Tran on 4/8/18.
//  Copyright © 2018 ltranco. All rights reserved.
//

import UIKit

class ProgressBarView: UIView {
    private var progressView: UIView = UIView()
    @IBInspectable var progressColor: UIColor = UIColor.red
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setUp()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setUp()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setUp()
    }
    
    private func setUp() {
        layer.masksToBounds = true
        layer.cornerRadius = 10
        layer.borderWidth = 4
        layer.borderColor = UIColor.white.cgColor
        backgroundColor = .clear
        
        progressView.frame = CGRect(x: 0, y: frame.height, width: frame.width, height: 0)
        progressView.backgroundColor = progressColor
        addSubview(progressView)
        sendSubview(toBack: progressView)
        setProgressTo(percentage: 0)
    }
    
    func reset() {
        progressView.frame = CGRect(x: 0, y: frame.height, width: frame.width, height: 0)
    }
    
    func setProgressTo(percentage: CGFloat, animated: Bool = true) {
        var p = max(percentage, 0)
        p = min(percentage, 100)
        
        let ceiling = frame.height * p
        var progress = min(0, self.progressView.frame.height)

        Timer.scheduledTimer(withTimeInterval: 0.005, repeats: true, block: {t in
            if progress >= ceiling {
                t.invalidate()
                return
            }
            self.progressView.frame.size.height = progress
            self.progressView.frame.origin.y = self.frame.height - progress
            progress += 1
        })
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        progressView.frame.size.width = frame.width
    }
}

public extension UIView {
    public func addAndPin(subview v: UIView) {
        v.translatesAutoresizingMaskIntoConstraints = false
        addSubview(v)
        addConstraints([
            NSLayoutConstraint(item: v, attribute: .centerX, relatedBy: .equal, toItem: self,
                               attribute: .centerX, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: v, attribute: .centerY, relatedBy: .equal, toItem: self,
                               attribute: .centerY, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: v, attribute: .width, relatedBy: .equal, toItem: self,
                               attribute: .width, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: v, attribute: .height, relatedBy: .equal, toItem: self,
                               attribute: .height, multiplier: 1, constant: 0)])
    }
}
