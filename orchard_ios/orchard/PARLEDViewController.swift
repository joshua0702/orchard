//
//  PARLEDViewController.swift
//  orchard
//
//  Created by Long Tran on 4/8/18.
//  Copyright © 2018 ltranco. All rights reserved.
//

import UIKit
import Charts

class LEDChartEntry: ChartDataEntry {
    private var maxY: Double = 0
    
    override init(x: Double, y: Double) {
        super.init(x: x, y: y)
        maxY = y * 1.05
    }
    
    func setProportionY(percentage: Double) {
        y = percentage * maxY
    }
    
    required init() {
        super.init()
    }
}

class VerticalSlider: UISlider {
    override init(frame: CGRect) {
        super.init(frame: frame)
        setUp()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setUp()
    }
    
    private func setUp() {
        transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi * -0.5))
        tintColor = .orange
    }
}

class PARLEDViewController: UIViewController, ChartViewDelegate {
    @IBOutlet weak var chart: LineChartView!
    @IBOutlet weak var globalSlider: UISlider!
    
    var p1 = LEDChartEntry(x: 450, y: 4.6)
    var p2 = LEDChartEntry(x: 500, y: 3.8)
    var p3 = LEDChartEntry(x: 560, y: 1.4)
    var p4 = LEDChartEntry(x: 610, y: 2.2)
    var p5 = LEDChartEntry(x: 680, y: 5.6)
    var p6 = LEDChartEntry(x: 770, y: 3.1)
    
    var c1 = VerticalSlider(frame: CGRect(x: -74, y: 572, width: 650, height: 10))
    var c2 = VerticalSlider(frame: CGRect(x: 117, y: 624, width: 550, height: 10))
    var c3 = VerticalSlider(frame: CGRect(x: 455, y: 798, width: 215, height: 10))
    var c4 = VerticalSlider(frame: CGRect(x: 545, y: 739, width: 320, height: 10))
    var c5 = VerticalSlider(frame: CGRect(x: 506, y: 504, width: 795, height: 10))
    var c6 = VerticalSlider(frame: CGRect(x: 935, y: 678, width: 450, height: 10))
    
    private let socket = OrchardSocketManager.shared
    
    @IBAction func globalSliderChanged(_ sender: UISlider) {
        let p: Double = Double(sender.value / sender.maximumValue)
        [p1, p2, p3, p4, p5, p6].forEach {
            $0.setProportionY(percentage: p)
        }
        
        let controls = [c1, c2, c3, c4, c5, c6]
        controls.forEach {
            $0.value = Float(p)
        }
        chart.notifyDataSetChanged()        
        socket.send(LEDS: controls.map({($0.tag, p)}))
    }
    
    func addControls() {
        for (i, c) in [c1, c2, c3, c4, c5, c6].enumerated() {
            c.tag = i
            c.alpha = 0
            c.value = 0.95
            c.maximumTrackTintColor = UIColor(displayP3Red: 32/255.0, green: 36/255.0, blue: 38/255.0, alpha: 1)
            c.addTarget(self, action: #selector(controlSliderChanged(_:)), for: .valueChanged)
            view.addSubview(c)
        }
    }

    @objc func controlSliderChanged(_ sender: UISlider) {
        let p: Double = Double(sender.value / sender.maximumValue)
        switch sender {
            case c1: p1.setProportionY(percentage: p)
            case c2: p2.setProportionY(percentage: p)
            case c3: p3.setProportionY(percentage: p)
            case c4: p4.setProportionY(percentage: p)
            case c5: p5.setProportionY(percentage: p)
            case c6: p6.setProportionY(percentage: p)
            default: break
        }
        chart.notifyDataSetChanged()
        socket.send(LEDS: [(sender.tag, p)])
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        globalSlider.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi * -0.5))
        
        addControls()
        
        chart.delegate = self
        chart.xAxis.labelPosition = .bottom
        chart.xAxis.axisLineColor = .white
        chart.xAxis.gridColor = .white
        chart.xAxis.labelTextColor = .white
        chart.leftAxis.axisLineColor = .white
        chart.leftAxis.gridColor = .white
        chart.leftAxis.labelTextColor = .white
        chart.leftAxis.granularity = 1
        chart.leftAxis.axisMinimum = 0
        chart.chartDescription = nil
        chart.rightAxis.enabled = false
        chart.legend.enabled = false
        chart.xAxis.drawGridLinesEnabled = false
        chart.isUserInteractionEnabled = false
        
        let pts = [
            ChartDataEntry(x: 400, y: 0.2),
            p1,
            ChartDataEntry(x: 485, y: 2.8),
            p2,
            ChartDataEntry(x: 540, y: 0.5),
            p3,
            ChartDataEntry(x: 585, y: 0.2),
            p4,
            ChartDataEntry(x: 640, y: 0.9),
            p5,
            ChartDataEntry(x: 740, y: 2.2),
            p6,
            ChartDataEntry(x: 800, y: 0.9)
        ]
        let dataSet = LineChartDataSet(values: pts, label: "???")
        dataSet.cubicIntensity = 0.3
        dataSet.mode = .horizontalBezier
        dataSet.colors = [UIColor(displayP3Red: 48/255.0, green: 54/255.0, blue: 57/255.0, alpha: 1)]
        let c = UIColor.orange
        dataSet.circleColors = [.clear, c]
        dataSet.circleHoleColor = .clear
        dataSet.lineWidth = 3
        
        let d = LineChartData(dataSet: dataSet)
        d.setDrawValues(false)
        
        chart.data = d
        chart.animate(xAxisDuration: 1, yAxisDuration: 1, easingOption: .easeInCubic)
        UIView.animate(withDuration: 2, animations: {
            [self.c1, self.c2, self.c3, self.c4, self.c5, self.c6].forEach {
                $0.alpha = 1
            }
        })
    }
}
