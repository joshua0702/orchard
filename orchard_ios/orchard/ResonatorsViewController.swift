//
//  ResonatorsViewController.swift
//  orchard
//
//  Created by Long Tran on 4/22/18.
//  Copyright © 2018 ltranco. All rights reserved.
//

import UIKit
import Lottie

class ResonatorsViewController: UIViewController {

    @IBOutlet weak var t1: UIView!
    @IBOutlet weak var t2: UIView!
    @IBOutlet weak var t3: UIView!
    @IBOutlet weak var t4: UIView!
    
    @IBOutlet weak var b1: UIView!
    @IBOutlet weak var b2: UIView!
    @IBOutlet weak var b3: UIView!
    @IBOutlet weak var b4: UIView!
    
    private var a1 = LOTAnimationView(name: "radar")
    private var a2 = LOTAnimationView(name: "radar")
    private var a3 = LOTAnimationView(name: "radar")
    private var a4 = LOTAnimationView(name: "radar")
    
    private var a5 = LOTAnimationView(name: "radar_green")
    private var a6 = LOTAnimationView(name: "radar_green")
    private var a7 = LOTAnimationView(name: "radar_green")
    private var a8 = LOTAnimationView(name: "radar_green")
    
    private let socket = OrchardSocketManager.shared
    
    override func viewDidLoad() {
        super.viewDidLoad()

        t1.addAndPin(subview: a1)
        t2.addAndPin(subview: a2)
        t3.addAndPin(subview: a3)
        t4.addAndPin(subview: a4)
        b1.addAndPin(subview: a5)
        b2.addAndPin(subview: a6)
        b3.addAndPin(subview: a7)
        b4.addAndPin(subview: a8)
        
        for (i, a) in [a1, a2, a3, a4, a5, a6, a7, a8].enumerated() {
            a.tag = i
            a.loopAnimation = true
            a.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(animationTap(_:))))
        }
    }
    
    @objc func animationTap(_ gesture: UITapGestureRecognizer) {
        if let v = gesture.view as? LOTAnimationView {
            v.isAnimationPlaying ? v.stop() : v.play()
            socket.sendResonator(ID: v.tag, state: v.isAnimationPlaying ? .ON : .OFF)
        }
    }

    @IBAction func onWaterChange(_ sender: UISwitch) {
        let animations = [a1, a2, a3, a4]
        animations.forEach {
            sender.isOn ? $0.play() : $0.stop()
        }
        socket.sendResonators(IDs: animations.map({ $0.tag }), state: sender.isOn ? .ON : .OFF)
    }
    
    @IBAction func onNutrientChange(_ sender: UISwitch) {
        let animations = [a5, a6, a7, a8]
        animations.forEach {
            sender.isOn ? $0.play() : $0.stop()
        }
        socket.sendResonators(IDs: animations.map({ $0.tag }), state: sender.isOn ? .ON : .OFF)
    }
}
